﻿using CuiseurRizAcme.IM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuiseurRizAcme.Core
{
    /// <summary>
    /// Permet de surveiller les changements de puissance du cuiseur pour en estimer la 
    /// température.
    /// </summary>
    class ContrôleurTempérature
    {
        /// <summary>
        /// Le contrôleur donne accès à la température et à la puissance de l'appareil.
        /// </summary>
        private readonly ContrôleurMatériel contrôleurMatériel = null;

        /// <summary>
        /// Contient la dernière puissance lue de l'interface
        /// </summary>
        private decimal puissanceActuelleEnWatts = 0;

        /// <summary>
        /// Spécifie depuis quand la puissance actuelle est définie.
        /// </summary>
        private DateTime débutPuissanceActuelle = DateTime.MinValue;

        /// <summary>
        /// Initialise une instance de CalculateurTempérature avec le contrôleurMatériel spécifié.
        /// </summary>
        /// <param name="contrôleurMatériel">Le contrôleur matériel qui permet d'obtenir la température</param>
        public ContrôleurTempérature(ContrôleurMatériel contrôleurMatériel)
        {
            this.contrôleurMatériel = contrôleurMatériel;
            this.puissanceActuelleEnWatts = contrôleurMatériel.LirePuissanceÉlémentEnWatts();
            this.débutPuissanceActuelle = DateTime.Now;
            this.contrôleurMatériel.ChangementPuissance += contrôleurMatériel_ChangementPuissance;
        }

        /// <summary>
        /// Estime la température du cuiseur en fonction de la puissance qui lui à été fournie
        /// </summary>
        void contrôleurMatériel_ChangementPuissance(object sender, EventArgs e)
        {
            //Calcule depuis combien de temps la puissance précédemment en place était active
            var duréePuissancePrécédenteEnSecondes = (decimal)(DateTime.Now - débutPuissanceActuelle).TotalSeconds;
           
            //Lit la quantité d'eau
            var qteEauL = contrôleurMatériel.LireQteEau();

            //Calcule la température générée en celcius depuis le dernier changement de puissance
            decimal températureGénérée = 0;

            if (puissanceActuelleEnWatts < 990)
            {
                températureGénérée = ((duréePuissancePrécédenteEnSecondes * -987) + (duréePuissancePrécédenteEnSecondes * puissanceActuelleEnWatts)) / (4200 * qteEauL);
            }
            else if (puissanceActuelleEnWatts != 0)
            {
                températureGénérée = (duréePuissancePrécédenteEnSecondes * puissanceActuelleEnWatts) / (4200 * qteEauL);
            }

            //Lit la température actuelle
            var températureActuelle = contrôleurMatériel.LireTempératureEnCelsius();

            //Applique la nouvelle température
            contrôleurMatériel.DéfinirTempératureEnCelcius(températureActuelle + températureGénérée);

            //Lire la nouvelle puissance
            puissanceActuelleEnWatts = contrôleurMatériel.LirePuissanceÉlémentEnWatts();
            débutPuissanceActuelle = DateTime.Now;
        }
    }
}
