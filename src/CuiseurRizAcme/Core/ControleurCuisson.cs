﻿using CuiseurRizAcme.IM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CuiseurRizAcme.Core
{
    /// <summary>
    /// Ce contrôleur permet de contrôler le cuiseur pendant un cycle de cuisson
    /// et permet de contrôler les cycles de cuisson
    /// </summary>
    public class ContrôleurCycleCuisson
    {
        private Cuiseur cuiseur { get; set; }

        /// <summary>
        /// Initialise une instance de ContrôleurCycleCuisson avec le cuiseur spécifié.
        /// </summary>
        /// <param name="cuiseur">Le cuiseur à controller</param>
        public ContrôleurCycleCuisson(Cuiseur cuiseur)
        {
            this.cuiseur = cuiseur;
        }

        /// <summary>
        /// Tente de démarrer un cycle de cuisson.
        /// </summary>
        /// <returns>Vrai si la cuisson est démarrée, sinon faux</returns>
        public bool DémarrerCuisson()
        {
            using (var timerCycle = new BackgroundWorker())
            using (var workerCycleCuisson = new BackgroundWorker())
            {
                //Exécute la tâche de surveillance du timer en arrière plan
                timerCycle.DoWork += delegate(object sender, DoWorkEventArgs e)
                {
                    while (true)
                    {
                        //Rapporte le progrès
                        timerCycle.ReportProgress(0);

                        //Arrêter le timer si la la cuisson est terminée.
                        if (cuiseur.Cycle == Cycle.Inactif)
                            break;

                        //Attendre un peu entre chaque boucle
                        Thread.Sleep(400);
                    }
                };

                //Exécute le cycle de cuisson en arrière plan 
                workerCycleCuisson.DoWork += delegate(object sender, DoWorkEventArgs e)
                {
                    DémmarerCycle();

                    //La boucle du cycle de cuisson
                    while (true)
                    {
                        //Si le cycle est terminé
                        if (CycleEstTerminé)
                        {
                            //Et qu'il n'y à plus de cycle après celui-ci
                            var nouveauCycle = TerminerCycle();
                            if (nouveauCycle == Cycle.Inactif)
                            {
                                //Arrêter le worker
                                break;
                            }
                            //Sinon continuer à traiter le cycle suivant
                        }

                        //Obtenir les règles de cuisson pour le cycle en cours
                        var règles = GetReglesCycle;

                        //Ajuster la puissance du cuiseur en fonction de la température visée
                        cuiseur.AjusterPuissance(règles);

                        //Rapporte le progrès
                        workerCycleCuisson.ReportProgress(0);

                        //Attendre un peu entre chaque boucle
                        Thread.Sleep(5000);
                    }
                };

                timerCycle.ProgressChanged += cycleWatcherWorker_ProgressChanged;
                workerCycleCuisson.ProgressChanged += cycleWatcherWorker_ProgressChanged;

                //La fin du cycle de cuisson
                workerCycleCuisson.RunWorkerCompleted += delegate(object sender, RunWorkerCompletedEventArgs e)
                {
                    cuiseur.OnChanged(EventArgs.Empty);
                };

                //Démarre un thread pour surveiller la puissance du cuiseur
                workerCycleCuisson.WorkerReportsProgress = true;
                workerCycleCuisson.RunWorkerAsync();

                //Démarre un thread pour surveiller le temps restantant au cycle.
                timerCycle.WorkerReportsProgress = true;
                timerCycle.RunWorkerAsync();
            }

            return false;
        }

        /// <summary>
        /// Arrête la cuisson
        /// </summary>
        /// <returns>Vrai si la cuisson est déjà arrêtée ou si elle a été arrêtée, sinon faux.</returns>
        public bool ArrêterCuisson()
        {
            //TODO
            return true;
        }

        /// <summary>
        /// Retourne vrai si le cycle courant est terminé, sinon retourne faux.
        /// </summary>
        private bool CycleEstTerminé
        {
            get
            {
                return GetSecondesRestantesAuCycle <= 0;
            }
        }


        /// <summary>
        /// Démarre le cycle de cuisson
        /// </summary>
        /// <returns></returns>
        private Cycle DémmarerCycle()
        {
            cuiseur.DébutDernierCycle = DateTime.Now;

            if (cuiseur.faireTremper)
                cuiseur.Cycle = Cycle.Trempage;
            else
                cuiseur.Cycle = Cycle.Cuisson;

            return cuiseur.Cycle;
        }


        /// <summary>
        /// Termine le cycle courant. Commence le cycle suivant si il y en as un, sinon arrête le cuiseur.
        /// </summary>
        /// <returns>Retourne le cycle suivant</returns>
        private Cycle TerminerCycle()
        {
            //Si c'est la fin de la cuisson
            if (cuiseur.Cycle == Cycle.Cuisson)
            {
                //Et qu'il faut garder le riz au chaud
                if (cuiseur.FaireRéchauffer)
                {
                    //Faire un cycle de réchauffage
                    cuiseur.Cycle = Cycle.Réchaud;
                    cuiseur.DébutDernierCycle = DateTime.Now;
                    return cuiseur.Cycle;
                }
            }

            //Si c'est la fin du trempage
            if (cuiseur.Cycle == Cycle.Trempage)
            {
                //Commencer la cuisson
                cuiseur.Cycle = Cycle.Cuisson;
                cuiseur.DébutDernierCycle = DateTime.Now;
                return cuiseur.Cycle;
            }

            //Sinon rendre le cuiseur inactif
            cuiseur.Cycle = Cycle.Inactif;
            cuiseur.DébutDernierCycle = DateTime.Now;
            return cuiseur.Cycle;
        }

        /// <summary>
        /// Retourne le nombre de secondes restantes au cycle au cycle en cours.
        /// Si le cuiseur est inactif, retourne 0 secondes.
        /// </summary>
        public int GetSecondesRestantesAuCycle
        {
            get
            {
                if (cuiseur.Cycle == Cycle.Inactif)
                    return 0;

                RegleCycle règlesCycle = GetReglesCycle;

                var duréeTotaleCycleEnSecondes = règlesCycle.DuréeEnMinutes * 60;
                TimeSpan tempsÉcoulé = DateTime.Now - cuiseur.DébutDernierCycle;

                return duréeTotaleCycleEnSecondes - (int)tempsÉcoulé.TotalSeconds;
            }
        }

        private RegleCycle GetReglesCycle
        {
            get
            {

                if (cuiseur.Cycle == Cycle.Trempage)
                {
                    return RegleCycle.GetReglePourTrempage();
                }

                if (cuiseur.Cycle == Cycle.Réchaud)
                {
                    return RegleCycle.GetReglePourRechauffage();
                }

                return RegleCycle.GetReglePourModeCuisson(cuiseur.ModeCuisson);
            }
        }

        /// <summary>
        /// Permet de déclancher cuiseur.OnChanged dans le thread du UI à partir d'un des thread de cuisson
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cycleWatcherWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            cuiseur.OnChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Permet de diminuer la temperature d'un maximum de 5 degre par minute
        /// </summary>
        /// <param name = ""></param>
        /// <param name =""></param>

        
    }
}
