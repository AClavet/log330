﻿using CuiseurRizAcme.Core;
using CuiseurRizAcme.IM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuiseurRizAcme.Core
{
    public enum Cycle { Inactif, Trempage, Cuisson, Réchaud };
    public enum ModeCuisson { Normal, Rapide, Lent, Aucun };

    public class Cuiseur
    {

        /// <summary>
        /// Cet événement est déclanché à chaque fois que l'état du cuiseur change.
        /// </summary>
        public event EventHandler ChangementEtatCuiseur;

        /// <summary>
        /// Le mode de cuisson sélectionné
        /// </summary>
        public ModeCuisson ModeCuisson { get; set; }

        /// <summary>
        /// Le cycle de cuisson auquel le cuiseur est.
        /// </summary>
        public Cycle Cycle { get; set; }

        /// <summary>
        /// Le moment où le cycle en cours à commencé.
        /// </summary>
        public DateTime DébutDernierCycle = DateTime.MinValue;

        /// <summary>
        /// Spécifie si il faut faire tremper le riz avant la cuisson.
        /// </summary>
        public bool faireTremper = false;

        /// <summary>
        /// Spécifie si il faut garder le riz au chaud après la cuisson.
        /// </summary>
        public bool faireRéchauffer = false;

        /// <summary>
        /// Donne accès aux ressources matérielles du cuiseur.
        /// </summary>
        private readonly ContrôleurMatériel contrôleurMatériel;


        ContrôleurCycleCuisson contrCycle = null;

        /// <summary>
        /// Initialise un nouveau cuiseur.
        /// </summary>
        public Cuiseur(ContrôleurMatériel contrôleurMatériel)
        {
            this.contrôleurMatériel = contrôleurMatériel;

            Cycle = Cycle.Inactif;
            ModeCuisson = ModeCuisson.Aucun;
        }

        /// <summary>
        /// Démarre un cycle de cuisson
        /// </summary>
        internal void DémarrerCuisson()
        {
            //Si le cuiseur est prêt
            if (EstPrêtADémmarrer)
            {
                //Prépare le cycle
                 contrCycle =  new ContrôleurCycleCuisson(this);

                //Démarre le cycle de cuisson en arrière plan
                contrCycle.DémarrerCuisson();
            }
        }

        /// <summary>
        /// Retourne le nombre de secondes restantes au cycle au cycle en cours.
        /// </summary>
        public int GetSecondesRestantesAuCycle
        {
            get
            {
                if (contrCycle != null)
                {
                    return contrCycle.GetSecondesRestantesAuCycle;
                }

                return 0;
            }
        }

        /// <summary>
        /// Propage l'événement 'ChangementEtatCuiseur'
        /// </summary>
        public virtual void OnChanged(EventArgs e)
        {
            if (ChangementEtatCuiseur != null)
                ChangementEtatCuiseur(this, e);
        }

        /// <summary>
        /// Tente de changer le mode de cuisson pour celui spécifé en paramètre.
        /// </summary>
        /// <param name="modeCuisson">Le nouveau mode de cuisson</param>
        internal void SetModeCuisson(ModeCuisson modeCuisson)
        {
            if (Cycle == Cycle.Inactif)
                ModeCuisson = modeCuisson;

            OnChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Spécifie si il faut faire tremper le riz ou non.
        /// </summary>
        /// <param name="faireTremper"></param>
        internal void FaireTremper(bool faireTremper)
        {
            switch (Cycle)
            {
                //Il est possible de changer l'option de trempage uniquement si le cuiseur est inactif.
                case Cycle.Inactif:
                    this.faireTremper = faireTremper;
                    break;
                default:
                    break;
            }

            OnChanged(EventArgs.Empty);
        }

        internal bool GetFaireTremper
        {
            get { return faireTremper; }
        }

        /// <summary>
        /// Spécifie si il faut faire réchauffer le riz ou non.
        /// </summary>
        /// <param name="faireRechauffer"></param>
        internal void FaireRechauffer(bool faireRechauffer)
        {
            switch (Cycle)
            {
                case Cycle.Inactif:
                case Cycle.Trempage:
                case Cycle.Cuisson:
                    this.faireRéchauffer = faireRechauffer;
                    break;
                default:
                    break;
            }

            OnChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Vérifie si le cuiseur est prêt à démmarrer un cycle.
        /// </summary>
        /// <returns>Vrai si le cuiseur est prêt, sinon faux.</returns>
        public bool EstPrêtADémmarrer
        {
            get
            {
                /*
                   Le cuiseur est prêt à démarrer un cycle si:
                   1. Il est inactif 
                   2. Un mode de cuisson autre que 'Aucun' est sélectionné.
                */
                return Cycle == Cycle.Inactif && ModeCuisson != ModeCuisson.Aucun;
            }
        }

        /// <summary>
        /// Vérifie si il est possible d'arrête le trempage au cycle en cours.
        /// </summary>
        public bool PeutArreterTrempage
        {
            get { return Cycle == Cycle.Inactif; }
        }

        /// <summary>
        /// Vérifie si il est possible de démarrer le trempage au cycle en cours.
        /// </summary>
        public bool PeutDemarrerTrempage
        {
            get { return Cycle == Cycle.Inactif; }
        }

        /// <summary>
        /// Ajuster la puissance du cuiseur en fonction des règles de cuisson.
        /// </summary>
        internal void AjusterPuissance(RegleCycle règles)
        {
            var maintenant = DateTime.Now;

            //Obtient la température cible pour cette étape du cycle
            var tempsÉcouléDébutCycleSecondes = (int)(maintenant - DébutDernierCycle).TotalSeconds;

            //Obtient la cible en fonction d'où le cycle est rendu
            var cible = règles.Cible(tempsÉcouléDébutCycleSecondes);
            if (cible == null)
                return;

            //Lit la température actuelle de l'élément
            var températureActuelleEnCelcius = contrôleurMatériel.LireTempératureEnCelsius();

            //Calcule l'écart entre la température cible et la température actuelle
            var écartDeTempérature = cible.températureEnDegrés - températureActuelleEnCelcius;

            //Calcule le temps restant pour atteindre la cible
            var tempsÉcouléDepuisDébutCible = (int)(maintenant - cible.départ).TotalSeconds;
            var tempsRestantEnSecondes = (cible.delaiEnMinutes * 60) - tempsÉcouléDepuisDébutCible;

            //Lire la qte d'eau
            var qteEauLitre = contrôleurMatériel.LireQteEau();

            decimal énergieEnJoules = 0;
            decimal puissanceEnWatts = 0;
            decimal puissancePerdueEnJoules = 0;

            // Si l'ecart de temperature est positif (vers le haut)
            if (écartDeTempérature > 0)
            {
                //Calcule l'énergie à générer pour atteindre la température cible
                énergieEnJoules = (qteEauLitre * 4200) * écartDeTempérature;

                //Calcule la puissance à fournir par seconde pour atteindre l'objectif
                puissanceEnWatts = énergieEnJoules / (decimal)tempsRestantEnSecondes;
            }
            else
            {
                // Valeur absolue de la variation de température
                écartDeTempérature = Math.Abs(écartDeTempérature);

                // Calcul de la perte de puissance en Watt de l'eau
                puissancePerdueEnJoules = 987 * tempsRestantEnSecondes;

                //Calcule l'énergie à générer pour prevenir une trop grande perte de chaleur et arrive a la temperature ciblee
                énergieEnJoules = ((puissancePerdueEnJoules / (qteEauLitre * 4200)) - écartDeTempérature) * (qteEauLitre * 4200);

                //Calcule la puissance à fournir par seconde pour atteindre l'objectif
                puissanceEnWatts = énergieEnJoules / (decimal)tempsRestantEnSecondes;
            }

            //Appliquer la nouvelle puissance
            contrôleurMatériel.DéfinirPuissanceÉlément(puissanceEnWatts);
        }

      

        public bool FaireRéchauffer
        {
            get
            {
                return faireRéchauffer;
            }
        }

        public decimal Puissance
        {
            get
            {
                return contrôleurMatériel.LirePuissanceÉlémentEnWatts();
            }
        }

        public decimal Température
        {
            get
            {
                return contrôleurMatériel.LireTempératureEnCelsius();
            }
        }
    }
}
