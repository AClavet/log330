﻿using CuiseurRizAcme.IM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuiseurRizAcme.Core
{
    /// <summary>
    /// Structure de donnée qui représente un objectif de température
    /// en un délai donné.
    /// </summary>
    public class Cible
    {
        public Cible()
        {
            this.delaiEnMinutes = 0;
            this.températureEnDegrés = 0;

        }
        public Cible(int températureEnDegrés, int delaiEnMinutes = 2)
        {
            this.delaiEnMinutes = delaiEnMinutes;
            this.températureEnDegrés = températureEnDegrés;
        }

        public int delaiEnMinutes = 0;
        public int températureEnDegrés = 0;

        public DateTime départ = DateTime.MinValue;
    }

    abstract class RegleCycle
    {
        public RegleCycle()
        {
            SignalSonore = false;
        }

        public int DuréeEnMinutes { get; set; }


        abstract public Cible Cible(int tempsEnMinute);

        /// <summary>
        /// Si vrai, le cuiseur doit effectuer un signal sonore à la fin du cycle.
        /// </summary>
        public bool SignalSonore { get; set; }

        public static RegleCycle GetReglePourModeCuisson(ModeCuisson mode)
        {
            switch (mode)
            {
                case ModeCuisson.Lent:   return new RègleCuissonLente();
                case ModeCuisson.Normal: return new RègleCuissonNormale();
                case ModeCuisson.Rapide: return new RègleCuissonRapide();
                case ModeCuisson.Aucun:
                default: return null;

            }
        }

        public static RegleCycle GetReglePourTrempage()
        {
            return new RègleTrempage();
        }

        public static RegleCycle GetReglePourRechauffage()
        {
            return new RègleRéchauffage();
        }
    }


    class RègleTrempage : RegleCycle
    {
        static Cible[] cibles = { new Cible(25) };
        
        public RègleTrempage()
        {
            //Le trempage dure une heure.
            DuréeEnMinutes = 60;
        }

        public override Cible Cible(int tempsEnSecondes)
        {
            //Si c'est la première cible
            if (tempsEnSecondes < cibles[0].delaiEnMinutes * 60)
            {
                if (cibles[0].départ == DateTime.MinValue)
                    cibles[0].départ = DateTime.Now;
                return cibles[0];
            }

            //Sinon déterminer la cible à laquelle le cuiseur est rendu
            int tempsÉcoulé = 0;
            foreach (var cible in cibles)
            {
                tempsÉcoulé += cible.delaiEnMinutes * 60;

                if (tempsEnSecondes < tempsÉcoulé)
                {
                    if (cible.départ == DateTime.MinValue)
                        cible.départ = DateTime.Now;
                    return cible;
                }
            }
            return null;
        }
    }

    class RègleRéchauffage : RegleCycle
    {
        static Cible[] cibles = { new Cible(80) };

        public RègleRéchauffage()
        {
            //Le trempage dure 4 heures (4 * 60 minutes)
            DuréeEnMinutes = 4 * 60;
        }

        public override Cible Cible(int tempsEnSecondes)
        {
            //Si c'est la première cible
            if (tempsEnSecondes < cibles[0].delaiEnMinutes * 60)
            {
                if (cibles[0].départ == DateTime.MinValue)
                    cibles[0].départ = DateTime.Now;
                return cibles[0];
            }

            //Sinon déterminer la cible à laquelle le cuiseur est rendu
            int tempsÉcoulé = 0;
            foreach (var cible in cibles)
            {
                tempsÉcoulé += cible.delaiEnMinutes * 60;

                if (tempsEnSecondes < tempsÉcoulé)
                {
                    if (cible.départ == DateTime.MinValue)
                        cible.départ = DateTime.Now;
                    return cible;
                }
            }
            return null;
        }
    }

    class RègleCuissonNormale : RegleCycle
    {
        /// <summary>
        /// Les cibles de température à atteindre.
        /// </summary>
        static Cible[] cibles = { new Cible(40), 
                                  new Cible(60),
                                  new Cible(70),
                                  new Cible(90),
                                  new Cible(100),
                                  new Cible(120),
                                  new Cible(110),
                                  new Cible(100),
                                  new Cible(90),
                                  new Cible(80),
                                  new Cible(75),
                                  new Cible(70),
                                  new Cible(70)};

        public RègleCuissonNormale()
        {
            /* Les cycles de cuisson on une durée de 30 minutes */
            DuréeEnMinutes = 30;

            SignalSonore = true;
        }

        public override Cible Cible(int tempsEnSecondes)
        {
            //Si c'est la première cible
            if (tempsEnSecondes < cibles[0].delaiEnMinutes * 60)
            {
                if (cibles[0].départ == DateTime.MinValue)
                    cibles[0].départ = DateTime.Now;
                return cibles[0];
            }
    
            //Sinon déterminer la cible à laquelle le cuiseur est rendu
            int tempsÉcoulé = 0;
            foreach (var cible in cibles)
            {
                tempsÉcoulé += cible.delaiEnMinutes * 60;

                if (tempsEnSecondes < tempsÉcoulé)
                {
                    if (cible.départ == DateTime.MinValue)
                        cible.départ = DateTime.Now;
                    return cible;
                }
            }
            return null;
        }
    }

    class RègleCuissonLente : RegleCycle
    {
        int[] températures = new int[30];

        static Cible[] cibles = { new Cible(25), 
                                  new Cible(40),
                                  new Cible(50),
                                  new Cible(70),
                                  new Cible(85),
                                  new Cible(100),
                                  new Cible(100),
                                  new Cible(90),
                                  new Cible(80),
                                  new Cible(80),
                                  new Cible(75),
                                  new Cible(70),
                                  new Cible(70)};

        public RègleCuissonLente()
        {
            /* Les cycles de cuisson on une durée de 30 minutes */
            DuréeEnMinutes = 30;

            SignalSonore = true;
        }

        public override Cible Cible(int tempsEnSecondes)
        {
            //Si c'est la première cible
            if (tempsEnSecondes < cibles[0].delaiEnMinutes * 60)
            {
                if (cibles[0].départ == DateTime.MinValue)
                    cibles[0].départ = DateTime.Now;
                return cibles[0];
            }

            //Sinon déterminer la cible à laquelle le cuiseur est rendu
            int tempsÉcoulé = 0;
            foreach (var cible in cibles)
            {
                tempsÉcoulé += cible.delaiEnMinutes * 60;

                if (tempsEnSecondes < tempsÉcoulé)
                {
                    if (cible.départ == DateTime.MinValue)
                        cible.départ = DateTime.Now;
                    return cible;
                }
            }
            return null;
        }
    }

    class RègleCuissonRapide : RegleCycle
    {
        int[] températures = new int[30];

        static Cible[] cibles = { new Cible(50), 
                                  new Cible(70),
                                  new Cible(90),
                                  new Cible(110),
                                  new Cible(100),
                                  new Cible(85),
                                  new Cible(84),
                                  new Cible(83),
                                  new Cible(81),
                                  new Cible(80),
                                  new Cible(75),
                                  new Cible(70),
                                  new Cible(70)};

        public RègleCuissonRapide()
        {
            /* Les cycles de cuisson on une durée de 30 minutes */
            DuréeEnMinutes = 30;

            SignalSonore = true;
        }

        public override Cible Cible(int tempsEnSecondes)
        {
            //Si c'est la première cible
            if (tempsEnSecondes < cibles[0].delaiEnMinutes * 60)
            {
                if (cibles[0].départ == DateTime.MinValue)
                    cibles[0].départ = DateTime.Now;
                return cibles[0];
            }

            //Sinon déterminer la cible à laquelle le cuiseur est rendu
            int tempsÉcoulé = 0;
            foreach (var cible in cibles)
            {
                tempsÉcoulé += cible.delaiEnMinutes * 60;

                if (tempsEnSecondes < tempsÉcoulé)
                {
                    if (cible.départ == DateTime.MinValue)
                        cible.départ = DateTime.Now;
                    return cible;
                }
            }
            return null;
        }
    }
    
}
