﻿using CuiseurRizAcme.Core;
using CuiseurRizAcme.IM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CuiseurRizAcme
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                //Assembler les composantes du système
                var contrôleurMatériel = new ContrôleurMatériel();
                var contrôleurTempérature = new ContrôleurTempérature(contrôleurMatériel);
                var cuiseur = new Cuiseur(contrôleurMatériel);
                var iu = new InterfacePrincipale(cuiseur);

                //Démarrer la boucle de l'interface utilisateur
                Application.Run(iu);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
    }
}
