﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuiseurRizAcme.IM
{
    /// <summary>
    /// Le contrôleur matériel permet de simuler l'interaction avec 
    /// les composantes matérielles du cuiseur.
    /// </summary>
    public class ContrôleurMatériel
    {
        /// <summary>
        /// La puissance maximale de l'élément chauffant.
        /// </summary>
        private static decimal PUISSANCE_MAX = 990;

        /// <summary>
        /// L'eau estimée dans le cuiseur.
        /// Pour fin de test, toujours considérer qu'il y à deux litres d'eau
        /// </summary>
        private decimal qteEauEnLitres = 2L;

        /// <summary>
        /// La température calculée de l'eau en celcius.
        /// </summary>
        private decimal températureEnCelcius = 25;

        /// <summary>
        /// La puissance en watts fournie à l'élément en ce moment
        /// </summary>
        private decimal puissanceÉlémentEnWatts = 0;

        /// <summary>
        /// Événement est déclanché à chaque fois la puissance de l'élément change.
        /// </summary>
        public event EventHandler ChangementPuissance;

        /// <summary>
        /// Consulter le thermomètre pour obtenir la température de l'eau.
        /// </summary>
        internal decimal LireTempératureEnCelsius()
        {
            return températureEnCelcius;
        }

        /// <summary>
        /// Définit la puissance à fournir en watts à l'élément.
        /// Si la puissance est plus grande que PUISSANCE_MAX, elle devient PUISSANCE_MAX.
        /// Si la puissance est plus petite que 0, elle devient 0.
        /// </summary>
        /// <param name="puissanceEnWatts">La puissance à fournir en watts.</param>
        internal void DéfinirPuissanceÉlément(decimal puissanceEnWatts)
        {
            if (puissanceEnWatts > PUISSANCE_MAX)
                puissanceEnWatts = PUISSANCE_MAX;

            if (puissanceEnWatts < 0)
                puissanceEnWatts = 0;

            puissanceÉlémentEnWatts = puissanceEnWatts;

            //Propage l'événement 'ChangementPuissance'
            if (ChangementPuissance != null)
                ChangementPuissance(this, EventArgs.Empty);
        }

        /// <summary>
        /// Consulte le matériel pour obtenir la puissance de l'élément
        /// </summary>
        /// <returns></returns>
        public decimal LirePuissanceÉlémentEnWatts()
        {
            return puissanceÉlémentEnWatts;
        }

        /// <summary>
        /// Retourne la quantité d'eau dans le cuiseur.
        /// </summary>
        /// <returns></returns>
        internal decimal LireQteEau()
        {
            return qteEauEnLitres;
        }

        /// <summary>
        /// Permet de définir la température. Pour fin de test uniquement, normalement
        /// c'est le matériel qui donne la température.
        /// </summary>
        /// <param name="températureEnCelcius"></param>
        internal void DéfinirTempératureEnCelcius(decimal températureEnCelcius)
        {
            this.températureEnCelcius = températureEnCelcius;
        }
    }
}
