﻿using CuiseurRizAcme.Core;
using CuiseurRizAcme.IM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CuiseurRizAcme
{   
    /// <summary>
    /// Cette interface logicielle simule l'interface matérielle du cuiseur de riz.
    /// </summary>
    public partial class InterfacePrincipale : Form
    {
        private Cuiseur cuiseur { get; set; }
  
        private string horlogeValeurDefaut = "88:88";

        /// <summary>
        /// Initialise l'interface avec le contrôleur de cuisson spécifié.
        /// </summary>
        /// <param name="controlleur"></param>
        public InterfacePrincipale(Cuiseur cuiseur)
        {
            this.cuiseur = cuiseur;

            //S'abonne aux changement d'état du cuiseur
            cuiseur.ChangementEtatCuiseur += SurChangementEtatCuiseur;

            InitializeComponent();

            RafraichirEtat();
        }

        void SurChangementEtatCuiseur(object sender, EventArgs e)
        {
            //Rafraîchir l'interface à chaque fois que l'état du cuiseur change.
            RafraichirEtat();
        }

        /// <summary>
        /// Ajuster l'interface utilisateur en fonction de l'état du cuiseur.
        /// </summary>
        private void RafraichirEtat()
        {
            SuspendLayout();

            //Boutons pour le mode de cuisson
            DesactiverBouton(btnCuissonNormale);
            DesactiverBouton(btnCuissonLente);
            DesactiverBouton(btnCuissonRapide);

            switch (cuiseur.ModeCuisson)
            {
                case ModeCuisson.Normal:
                    ActiverBouton(btnCuissonNormale);
                    break;
                case ModeCuisson.Rapide:
                    ActiverBouton(btnCuissonRapide);
                    break;
                case ModeCuisson.Lent:
                    ActiverBouton(btnCuissonLente);
                    break;
                case ModeCuisson.Aucun:
                    break;
                default:
                    break;
            }
                
            //Indicateurs pour le cycle
            cbIndicateurCuisson.Checked = false;
            cbIndicateurRechaud.Checked = false;
            cbIndicateurTrempage.Checked = false;

            switch (cuiseur.Cycle)
            {
                case Cycle.Trempage:
                    cbIndicateurTrempage.Checked = true;
                    break;
                case Cycle.Cuisson:
                    cbIndicateurCuisson.Checked = true;
                    break;
                case Cycle.Réchaud:
                    cbIndicateurRechaud.Checked = true;
                    break;
                default:
                    break;
            }

            //Boutons pour le trempage et le réchaud
            DesactiverBouton(btnTrempage);
            DesactiverBouton(btnRéchauffer);

            if (cuiseur.FaireRéchauffer)
            {
                ActiverBouton(btnRéchauffer);
            }

            btnTrempage.Enabled = cuiseur.PeutDemarrerTrempage;
            if (cuiseur.GetFaireTremper)
            {
                btnTrempage.Enabled = cuiseur.PeutArreterTrempage;
                ActiverBouton(btnTrempage);
            }

            //Bouton pour démarrer la cuisson
            btnSurClicDemarrerCuisson.Enabled = cuiseur.EstPrêtADémmarrer;
               
            //Temps restant au cycle
            var secondesTotales = cuiseur.GetSecondesRestantesAuCycle;
            
            //Si il n'y a pas de temps d'affiché
            if (secondesTotales == 0)
            {
                //Afficher la valeur par défaut
                labelHorloge.Text = horlogeValeurDefaut;
            }
            else
            {
                //Sinon afficher le temps restant en format hh:MM
                TimeSpan t = TimeSpan.FromSeconds(secondesTotales);
                labelHorloge.Text = string.Format("{0:D2}:{1:D2}", t.Hours, t.Minutes);

                //Sinon afficher le temps restant en format mm:ss
               // var mins = ~~(secondesTotales / 60);
               // var secs = secondesTotales % 60;
            }

            //Afficher la puissance
            labelPuissance.Text = cuiseur.Puissance.ToString("N2") + " W";

            //Afficher la température
            labelTempérature.Text = cuiseur.Température.ToString("N2") + " C";

            ResumeLayout();
        }

        private void DefinirModeCuisson(ModeCuisson mode, Button bouton)
        {
            switch (bouton.FlatStyle)
            {
                case FlatStyle.Flat:
                    cuiseur.SetModeCuisson(ModeCuisson.Aucun);
                    break;
                case FlatStyle.Standard:
                    cuiseur.SetModeCuisson(mode);
                    break;
            }
        }

        private void btnModeCuissonLente_Click(object sender, EventArgs e)
        {
            DefinirModeCuisson(ModeCuisson.Lent, btnCuissonLente);
        }

        private void btnModeNormal_Click(object sender, EventArgs e)
        {
            DefinirModeCuisson(ModeCuisson.Normal, btnCuissonNormale);
        }

        private void btnCuissonRapide_Click(object sender, EventArgs e)
        {
            DefinirModeCuisson(ModeCuisson.Rapide, btnCuissonRapide);
        }

        private void btnDemarrerCuisson_Click(object sender, EventArgs e)
        {
            cuiseur.DémarrerCuisson();
        }

        private void btnTrempage_Click(object sender, EventArgs e)
        {
            if (btnTrempage.FlatStyle == FlatStyle.Standard)
                cuiseur.FaireTremper(true);
            else if (btnTrempage.FlatStyle == FlatStyle.Flat)
                cuiseur.FaireTremper(false);
        }

        private void btnRéchauffer_Click(object sender, EventArgs e)
        {
            if (btnRéchauffer.FlatStyle == FlatStyle.Standard)
                cuiseur.FaireRechauffer(true);
            else if (btnRéchauffer.FlatStyle == FlatStyle.Flat)
                cuiseur.FaireRechauffer(false);
        }


        private void DesactiverBouton(Button bouton)
        {
            bouton.FlatStyle = FlatStyle.Standard;
        }

        private void ActiverBouton(Button bouton)
        {
            bouton.FlatStyle = FlatStyle.Flat;
        }
    }
}
