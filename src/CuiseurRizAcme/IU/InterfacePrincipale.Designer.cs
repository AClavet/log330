﻿namespace CuiseurRizAcme
{
    partial class InterfacePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSurClicDemarrerCuisson = new System.Windows.Forms.Button();
            this.btnCuissonNormale = new System.Windows.Forms.Button();
            this.btnCuissonLente = new System.Windows.Forms.Button();
            this.cbIndicateurCuisson = new System.Windows.Forms.CheckBox();
            this.btnCuissonRapide = new System.Windows.Forms.Button();
            this.cbIndicateurTrempage = new System.Windows.Forms.CheckBox();
            this.cbIndicateurRechaud = new System.Windows.Forms.CheckBox();
            this.btnTrempage = new System.Windows.Forms.Button();
            this.btnRéchauffer = new System.Windows.Forms.Button();
            this.labelHorloge = new System.Windows.Forms.Label();
            this.labelPuissance = new System.Windows.Forms.Label();
            this.labelTempérature = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSurClicDemarrerCuisson
            // 
            this.btnSurClicDemarrerCuisson.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSurClicDemarrerCuisson.Location = new System.Drawing.Point(12, 194);
            this.btnSurClicDemarrerCuisson.Name = "btnSurClicDemarrerCuisson";
            this.btnSurClicDemarrerCuisson.Size = new System.Drawing.Size(101, 33);
            this.btnSurClicDemarrerCuisson.TabIndex = 0;
            this.btnSurClicDemarrerCuisson.Text = "Départ";
            this.btnSurClicDemarrerCuisson.UseVisualStyleBackColor = true;
            this.btnSurClicDemarrerCuisson.Click += new System.EventHandler(this.btnDemarrerCuisson_Click);
            // 
            // btnCuissonNormale
            // 
            this.btnCuissonNormale.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCuissonNormale.Location = new System.Drawing.Point(12, 47);
            this.btnCuissonNormale.Name = "btnCuissonNormale";
            this.btnCuissonNormale.Size = new System.Drawing.Size(123, 34);
            this.btnCuissonNormale.TabIndex = 1;
            this.btnCuissonNormale.Text = "Normal";
            this.btnCuissonNormale.UseVisualStyleBackColor = true;
            this.btnCuissonNormale.Click += new System.EventHandler(this.btnModeNormal_Click);
            // 
            // btnCuissonLente
            // 
            this.btnCuissonLente.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCuissonLente.Location = new System.Drawing.Point(12, 9);
            this.btnCuissonLente.Name = "btnCuissonLente";
            this.btnCuissonLente.Size = new System.Drawing.Size(123, 33);
            this.btnCuissonLente.TabIndex = 2;
            this.btnCuissonLente.Text = "Lent";
            this.btnCuissonLente.UseVisualStyleBackColor = true;
            this.btnCuissonLente.Click += new System.EventHandler(this.btnModeCuissonLente_Click);
            // 
            // cbIndicateurCuisson
            // 
            this.cbIndicateurCuisson.AutoSize = true;
            this.cbIndicateurCuisson.Enabled = false;
            this.cbIndicateurCuisson.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIndicateurCuisson.Location = new System.Drawing.Point(296, 48);
            this.cbIndicateurCuisson.Name = "cbIndicateurCuisson";
            this.cbIndicateurCuisson.Size = new System.Drawing.Size(119, 33);
            this.cbIndicateurCuisson.TabIndex = 3;
            this.cbIndicateurCuisson.Text = "Cuisson";
            this.cbIndicateurCuisson.UseVisualStyleBackColor = true;
            // 
            // btnCuissonRapide
            // 
            this.btnCuissonRapide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCuissonRapide.Location = new System.Drawing.Point(12, 86);
            this.btnCuissonRapide.Name = "btnCuissonRapide";
            this.btnCuissonRapide.Size = new System.Drawing.Size(123, 34);
            this.btnCuissonRapide.TabIndex = 4;
            this.btnCuissonRapide.Text = "Rapide";
            this.btnCuissonRapide.UseVisualStyleBackColor = true;
            this.btnCuissonRapide.Click += new System.EventHandler(this.btnCuissonRapide_Click);
            // 
            // cbIndicateurTrempage
            // 
            this.cbIndicateurTrempage.AutoSize = true;
            this.cbIndicateurTrempage.Enabled = false;
            this.cbIndicateurTrempage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIndicateurTrempage.Location = new System.Drawing.Point(296, 9);
            this.cbIndicateurTrempage.Name = "cbIndicateurTrempage";
            this.cbIndicateurTrempage.Size = new System.Drawing.Size(145, 33);
            this.cbIndicateurTrempage.TabIndex = 5;
            this.cbIndicateurTrempage.Text = "Trempage";
            this.cbIndicateurTrempage.UseVisualStyleBackColor = true;
            // 
            // cbIndicateurRechaud
            // 
            this.cbIndicateurRechaud.AutoSize = true;
            this.cbIndicateurRechaud.Enabled = false;
            this.cbIndicateurRechaud.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIndicateurRechaud.Location = new System.Drawing.Point(296, 87);
            this.cbIndicateurRechaud.Name = "cbIndicateurRechaud";
            this.cbIndicateurRechaud.Size = new System.Drawing.Size(128, 33);
            this.cbIndicateurRechaud.TabIndex = 6;
            this.cbIndicateurRechaud.Text = "Réchaud";
            this.cbIndicateurRechaud.UseVisualStyleBackColor = true;
            // 
            // btnTrempage
            // 
            this.btnTrempage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrempage.Location = new System.Drawing.Point(141, 9);
            this.btnTrempage.Name = "btnTrempage";
            this.btnTrempage.Size = new System.Drawing.Size(149, 33);
            this.btnTrempage.TabIndex = 7;
            this.btnTrempage.Text = "Tremper";
            this.btnTrempage.UseVisualStyleBackColor = true;
            this.btnTrempage.Click += new System.EventHandler(this.btnTrempage_Click);
            // 
            // btnRéchauffer
            // 
            this.btnRéchauffer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRéchauffer.Location = new System.Drawing.Point(141, 87);
            this.btnRéchauffer.Name = "btnRéchauffer";
            this.btnRéchauffer.Size = new System.Drawing.Size(149, 34);
            this.btnRéchauffer.TabIndex = 8;
            this.btnRéchauffer.Text = "Réchauffer";
            this.btnRéchauffer.UseVisualStyleBackColor = true;
            this.btnRéchauffer.Click += new System.EventHandler(this.btnRéchauffer_Click);
            // 
            // labelHorloge
            // 
            this.labelHorloge.AutoSize = true;
            this.labelHorloge.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelHorloge.Font = new System.Drawing.Font("Dungeon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHorloge.Location = new System.Drawing.Point(119, 195);
            this.labelHorloge.Name = "labelHorloge";
            this.labelHorloge.Size = new System.Drawing.Size(101, 32);
            this.labelHorloge.TabIndex = 9;
            this.labelHorloge.Text = "88:88";
            // 
            // labelPuissance
            // 
            this.labelPuissance.AutoSize = true;
            this.labelPuissance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelPuissance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPuissance.Location = new System.Drawing.Point(296, 149);
            this.labelPuissance.Name = "labelPuissance";
            this.labelPuissance.Size = new System.Drawing.Size(56, 33);
            this.labelPuissance.TabIndex = 10;
            this.labelPuissance.Text = "0W";
            // 
            // labelTempérature
            // 
            this.labelTempérature.AutoSize = true;
            this.labelTempérature.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelTempérature.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTempérature.Location = new System.Drawing.Point(296, 195);
            this.labelTempérature.Name = "labelTempérature";
            this.labelTempérature.Size = new System.Drawing.Size(51, 33);
            this.labelTempérature.TabIndex = 11;
            this.labelTempérature.Text = "0C";
            // 
            // InterfacePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 239);
            this.Controls.Add(this.labelTempérature);
            this.Controls.Add(this.labelPuissance);
            this.Controls.Add(this.labelHorloge);
            this.Controls.Add(this.btnRéchauffer);
            this.Controls.Add(this.btnTrempage);
            this.Controls.Add(this.cbIndicateurRechaud);
            this.Controls.Add(this.cbIndicateurTrempage);
            this.Controls.Add(this.btnCuissonRapide);
            this.Controls.Add(this.cbIndicateurCuisson);
            this.Controls.Add(this.btnCuissonLente);
            this.Controls.Add(this.btnCuissonNormale);
            this.Controls.Add(this.btnSurClicDemarrerCuisson);
            this.Name = "InterfacePrincipale";
            this.Text = "Cuiseur de riz";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSurClicDemarrerCuisson;
        private System.Windows.Forms.Button btnCuissonNormale;
        private System.Windows.Forms.Button btnCuissonLente;
        private System.Windows.Forms.CheckBox cbIndicateurCuisson;
        private System.Windows.Forms.Button btnCuissonRapide;
        private System.Windows.Forms.CheckBox cbIndicateurTrempage;
        private System.Windows.Forms.CheckBox cbIndicateurRechaud;
        private System.Windows.Forms.Button btnTrempage;
        private System.Windows.Forms.Button btnRéchauffer;
        private System.Windows.Forms.Label labelHorloge;
        private System.Windows.Forms.Label labelPuissance;
        private System.Windows.Forms.Label labelTempérature;
    }
}

