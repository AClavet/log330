#Code source du cuiseur de riz

##Utilisation
Pour ouvrir la solution il faut [Visual Studio 2012](http://www.microsoft.com/visualstudio/eng/downloads) et le framework .NET 4.0+. 

##Ex�cution
Pour ex�cuter le programme directement, utiliser le programme [d�ja compil�](https://bitbucket.org/AClavet/log330/src/master/src/CuiseurRizAcme/exe/CuiseurRizAcme.exe)


##Strat�gie de branches
La m�thode simple est utilis�e. Tous les d�veloppeurs utilisent la branche principale pour travailler.  Quand une version du logiciel doit �tre livr�e,
un [tag](http://learn.github.com/p/tagging.html) est donn� au commit associ� pour le figer.

### La structure
La branche principale (master)  
    - commit 1  
    - commit 2 (tag v1.0)  
    - commit 3  
    - commit x (tag 1.x)
